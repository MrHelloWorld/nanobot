Простенький бот для мониторинга хешрейта кошелька nanopool.org с отправкой уведомления в Telegram

1. Rename & change _config.php => config.php
2. In ./bot.php (не вынесено в config):
    - [minHashrate - минимальный нормальный хешрейт.]
    - [alarmReportInterval - отправка уведомления только 1 раз  в ххх сек.]
    - [warningDurationBeforeAlarm - необходимая длительность сниженного хешрейта для начала отправки алармов.]
    - [Можно удалить ненужные ссобщения на экран, файл и проч]
3. Start:  php ./bot.php 


