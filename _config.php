<?php

return [
    'app' => [
        "telegram_token" => "*:*",
        "reply_chat_id" => 1
    ],

    'logger' => [
        "logPath" => 'nanobot.log',
    ],

    'wallets' => [
        'eth' => ['eth' => '0xf0...'],
        'etc' =>['etc' => '0xda...'],
    ]
];