<?php

require "vendor/autoload.php";
//require "App/Nanobot.php";

use App\Helper;
use App\Nanobot;
use App\Watchers\NanopoolReportedHashrateWatcher;
use App\Notifiers\TelegramNotifier;
use App\Notifiers\ConsoleNotifier;
use App\Notifiers\LogFileNotifier;

$wallets = Helper::getParams('wallets');

$bot = new Nanobot();

$bot->addNotifier(new TelegramNotifier());
$bot->addNotifier(new LogFileNotifier());
$bot->addNotifier(new ConsoleNotifier());
$bot->addWatcher(new NanopoolReportedHashrateWatcher([
    'wallet' => $wallets['eth'],
    'minHashrate' => '500',
    'alarmReportInterval' => 60*30,
    'warningDurationBeforeAlarm' => 60*7
]));
$bot->run();
