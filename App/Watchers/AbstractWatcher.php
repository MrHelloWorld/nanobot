<?php

namespace App\Watchers;

abstract class AbstractWatcher
{
    protected array $params;

    protected int $lastAlarmTimeStamp = 0;
    protected int $alarmReportInterval;

    protected int $warningTimeStamp = 0;
    protected int $warningDurationBeforeAlarm = 0;

    public WatcherStatus $status;

    abstract public function init(): bool;
    abstract public function watch(): WatcherStatus;


    public function __construct($params = null)
    {
        $this->params = $params;
        $this->status = new WatcherStatus();
        $this->alarmReportInterval = $params['alarmReportInterval'] ?? 60*30;
        $this->warningDurationBeforeAlarm = $params['warningDurationBeforeAlarm'] ?? 60*70;
    }

    public function setWarningStatus(string $warning)
    {
        $this->status->setMessage($warning);
        $now = time();

        if ($this->status->isOk()) {
            $this->warningTimeStamp = $now;
            $this->status->setWarning();
        }

        if ($this->status->isWarning()) {
            if ($now - $this->warningTimeStamp > $this->warningDurationBeforeAlarm) {
                $this->status->setAlarm();
            }
        }
        if ($this->status->isAlarm() && $this->needToSendAlarm()) {
                $this->status->setNeedToSendAlarm();
        }
    }

    public function setNormalStatus(string $message)
    {
        $this->status->setMessage($message);
        $this->status->setOk();
        $this->warningTimeStamp = 0;
    }

    public function setBackToNormalStatus(string $message)
    {
        $this->status->setMessage($message);
        $this->status->setBackToNormal();
        $this->warningTimeStamp = 0;
    }

    public function alarmWasSent()
    {
        $this->status->setAlarm();
        $this->lastAlarmTimeStamp = time();
    }

    public function needToSendAlarm()
    {
        return ($this->lastAlarmTimeStamp + $this->alarmReportInterval) < time();
    }

    public function setReportAlarmInterval(int $seconds)
    {
        $this->alarmReportInterval = $seconds;
    }
}
