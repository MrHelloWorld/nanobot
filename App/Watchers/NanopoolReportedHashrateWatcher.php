<?php

namespace App\Watchers;

use App\Notifiers\Notify;
use App\Watchers\WatcherStatus;

class NanopoolReportedHashrateWatcher extends AbstractWatcher
{
    use NanopoolApi;
    use Notify;

    protected int $reportedHashrate = 0;

    public function init(): bool
    {
        $this->addNanopoolWallet($this->params['wallet']);

        return true;
    }

    public function watch(): WatcherStatus
    {
        $response = $this->getReportedHashrateResponse();
        if ($response->status == false) {
            $this->setWarningStatus($this->getStringWallet() . '. ' . $response->data);
        } else {
            $this->reportedHashrate = $response->data;
            if ($this->reportedHashrate < $this->params['minHashrate']) {
                $this->setWarningStatus($this->getStringWallet() . ". Hashrate is less than needed: {$this->reportedHashrate} of {$this->params['minHashrate']}");
            } else {
                if ($this->status->isAlarm() || $this->status->isNeedToSendAlarm()) {
                    $this->setBackToNormalStatus("Back to noraml hashrate: {$this->reportedHashrate} of {$this->params['minHashrate']}");
                } else {
                    $this->setNormalStatus("Current reported hashrate is {$this->reportedHashrate}");
                }
            }
        }
        return $this->status;
    }
}
