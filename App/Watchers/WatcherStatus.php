<?php

namespace App\Watchers;

class WatcherStatus
{
    public const STATUS_OK = 1;
    public const STATUS_WARNING_IN_PROGRESS = 2;
    public const STATUS_ALARM = 3;
    public const STATUS_NEED_TO_SEND_ALARM = 4;
    public const STATUS_BACK_TO_NORMAL = 5;

    protected int $status = self::STATUS_OK;
    protected string $message = "";

    public function toString(): string
    {
        $str = "Status: ";
        switch ($this->status)
        {
            case self::STATUS_OK:
                $str .= "OK. ";
                break;

            case self::STATUS_WARNING_IN_PROGRESS:
                $str .= "WARNING. ";
                break;

            case self::STATUS_ALARM:
                $str .= "ALARM. ";
                break;

            case self::STATUS_NEED_TO_SEND_ALARM:
                $str .= "NEED TO SEND ALARM. ";
                break;

            case self::STATUS_BACK_TO_NORMAL:
                $str .= "BACK TO NORMAL. ";
                break;
        }
        return "$str Message: {$this->message}\n";
    }

    public function setWarning()
    {
        $this->status = self::STATUS_WARNING_IN_PROGRESS;
    }

    public function setOk()
    {
        $this->status = self::STATUS_OK;
    }

    public function setAlarm()
    {
        $this->status = self::STATUS_ALARM;
    }

    public function setNeedToSendAlarm()
    {
        $this->status = self::STATUS_NEED_TO_SEND_ALARM;
    }

    public function setBackToNormal()
    {
        $this->status = self::STATUS_BACK_TO_NORMAL;
    }

    public function isAlarm(): bool
    {
        return $this->status === self::STATUS_ALARM;
    }

    public function isNeedToSendAlarm(): bool
    {
        return $this->status === self::STATUS_NEED_TO_SEND_ALARM;
    }

    public function isOk(): bool
    {
        return $this->status === self::STATUS_OK;
    }

    public function isWarning(): bool
    {
        return $this->status === self::STATUS_WARNING_IN_PROGRESS;
    }

    public function isbackToNormal(): bool
    {
        return $this->status === self::STATUS_BACK_TO_NORMAL;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}