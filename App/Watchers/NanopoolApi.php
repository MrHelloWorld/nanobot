<?php

namespace App\Watchers;

use App\Helper;

trait NanopoolApi
{
    protected array $nanopoolWallet;

    public function addNanopoolWallet(array $wallet)
    {
        $this->nanopoolWallet = $wallet;
    }

    public function getReportedHashrateResponse()
    {
        return json_decode(file_get_contents(
            $this->getApiUrl(
                key($this->nanopoolWallet),
                "reportedhashrate",
                [array_values($this->nanopoolWallet)[0]]
            )
        ));
    }

    protected function getApiUrl(string $coin, string $funcName, array $params): string
    {
        return  "https://api.nanopool.org/v1/{$coin}/{$funcName}/" . implode("/", $params);
    }

    protected function getStringWallet()
    {
        return key($this->nanopoolWallet).':'.array_values($this->nanopoolWallet)[0];
    }
}
