<?php

namespace App;

use App\Logger;
use App\Helper;
use App\Notifiers\NotifyInterface;
use App\Watchers\AbstractWatcher;
use App\Watchers\WatcherStatus;

class Nanobot
{
    protected Logger $logger;
    protected array $params;
    protected array $watchers = [];
    protected array $notifiers = [];

    public function __construct()
    {
        $loger = new Logger();
        $loger->LogEvent("Bot started");
    }

    protected function init()
    {
        foreach ($this->watchers as $watcher) {
            $watcher->init();
            foreach ($this->notifiers as $notifier) {
                $watcher->addNotifier($notifier);
            }
        }
    }

    public function run()
    {
        $this->init();
        while (true) {
            foreach ($this->watchers as $watcher) {
                $status = $watcher->watch();
                //echo $status->toString();
                if ($status->isNeedToSendAlarm()) {
                    $watcher->notifyError($status->getMessage());
                    $watcher->alarmWasSent();
                } else if ($status->isBackToNormal()) {
                    $watcher->notify($status->getMessage());
                }
                sleep(1);
            }
        }
    }

    public function addWatcher(AbstractWatcher $watcher)
    {
        $this->watchers[] = $watcher;
    }

    public function addNotifier(NotifyInterface $notifier)
    {
        $this->notifiers[] = $notifier;
    }
}
