<?php

namespace App\Notifiers;

use App\Helper;
use App\Notifiers\NotifyInterface;
use TelegramBot\Api\BotApi;

class TelegramNotifier implements NotifyInterface
{
    protected BotApi $bot;

    public function __construct()
    {
        $this->params = Helper::getParams('app');
        $this->bot = new BotApi($this->params['telegram_token']);
    }

    public function sendAlarmMessage(string $message)
    {
        $this->bot->sendMessage($this->params['reply_chat_id'], $message);
    }

    public function sendMessage(string $message)
    {
        $this->bot->sendMessage($this->params['reply_chat_id'], $message);
    }
}