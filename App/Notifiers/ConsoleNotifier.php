<?php

namespace App\Notifiers;

use App\Notifiers\NotifyInterface;

class ConsoleNotifier implements NotifyInterface
{

    public function sendAlarmMessage(string $message)
    {
        echo "Alarm: $message\n";
    }

    public function sendMessage(string $message)
    {
        echo "Hint: $message\n";
    }
}