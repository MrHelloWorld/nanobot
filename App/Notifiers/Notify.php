<?php

namespace App\Notifiers;

use App\Notifiers\NotifyInterface;

trait Notify
{
    protected array $notifiers = [];

    public function addNotifier(NotifyInterface $notifier)
    {
        $this->notifiers[] = $notifier;
        //var_dump()
    }

    public function notifyError(string $error)
    {
        foreach ($this->notifiers as $ntf) {
            $ntf->sendAlarmMessage($error);
        }
    }

    public function notify(string $error)
    {
        foreach ($this->notifiers as $ntf) {
            $ntf->sendMessage($error);
        }
    }
}