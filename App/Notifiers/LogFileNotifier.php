<?php

namespace App\Notifiers;

use App\Logger;
use App\Notifiers\NotifyInterface;

class LogFileNotifier implements NotifyInterface
{
    protected Logger $logger;

    public function __construct()
    {
        $this->logger = new Logger();
        $this->logger->LogEvent("Bot started");
    }

    public function sendAlarmMessage(string $message)
    {
        $this->logger->LogEvent("Alarm: $message");
    }

    public function sendMessage(string $message)
    {
        $this->logger->LogEvent("Hint: $message");
    }
}