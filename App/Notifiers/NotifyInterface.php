<?php

namespace App\Notifiers;

interface NotifyInterface
{
    public function sendAlarmMessage(string $message);

    public function sendMessage(string $message);

}