<?php

namespace App;

use App\Helper;

class Logger
{
    protected $params;
    protected $file;

    public function __construct()
    {
        $this->params = Helper::getParams('logger');

        $this->file = fopen($this->params["logPath"], "w+");
    }

    public function LogEvent(string $event)
    {
        fputs($this->file, $event.PHP_EOL);
    }
}